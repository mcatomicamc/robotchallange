﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Dmytro.Rudevych.RobotChallange.Test
{
    [TestClass]
    public class MovingTest
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new DmytroRudevychAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1,1);
            map.Stations.Add(new EnergyStation(){Energy = 1000,Position = stationPosition,RecoveryRate = 2});
            var robots = new List<Robot.Common.Robot>(){new Robot.Common.Robot(){Energy = 200,Position = new Position(2,3)}};
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is MoveCommand);
        }
    }
}
