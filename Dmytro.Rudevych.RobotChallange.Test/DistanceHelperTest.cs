﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;


namespace Dmytro.Rudevych.RobotChallange.Test
{
    [TestClass]
    public class DistanceHelperTest
    {
        [TestMethod]
        public void DistanceTest()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(3,4);
            Assert.AreEqual(13,DistanceHelper.FindDistance(p1,p2));
        }
    }
}
