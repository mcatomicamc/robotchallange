﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Dmytro.Rudevych.RobotChallange;

namespace Dmytro.Rudevych.RobotChallange.Test
{
    [TestClass]
    public class IsCellFreeTest
    {
        [TestMethod]
        public void FreeCellTest()
        {
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, OwnerName = "friend", Position = new Position(1, 1) } };
            robots.Add(new Robot.Common.Robot() { Energy = 200, OwnerName = "friend", Position = new Position(2, 2) });
            Position pos = new Position(2,2);
            var algorihm = new DmytroRudevychAlgorithm();
            Assert.IsTrue(!algorihm.IsCellFree(pos, robots[0], robots));
        }
    }
}
