﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Dmytro.Rudevych.RobotChallange;

namespace Dmytro.Rudevych.RobotChallange.Test
{
    [TestClass]
    public class NearestFreeStationTest
    {
        [TestMethod]
        public void NearestStFreeTest()
        {
            
            var map = new Map();
            var stationPosition = new Position(5, 7);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var stationPosition2 = new Position(6, 4);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition2, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) } };
            var algorithm = new DmytroRudevychAlgorithm();
            Position nearest = algorithm.FindNearestFreeStation(robots[0], map, robots);
            Assert.AreEqual(stationPosition2,nearest);

        }
    }
}
