﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Dmytro.Rudevych.RobotChallange;

namespace Dmytro.Rudevych.RobotChallange.Test
{
    [TestClass]
    public class AtackingFriendTest
    {
        [TestMethod]
        public void FriendAtackTest()
        {
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, OwnerName = "friend", Position = new Position(1, 1) } };
            robots.Add(new Robot.Common.Robot() { Energy = 200, OwnerName = "friend", Position = new Position(2, 2) });

            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(2, 2), RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(10, 10), RecoveryRate = 2 });

            var algorithm = new DmytroRudevychAlgorithm();
            Position freeStationPosition = algorithm.FindNearestFreeStation(robots[0], map, robots);
            Position busyStationPosition = new Position(10,10);


            Position nextStepPosition = new Position();
            int toFreeDistance = DistanceHelper.FindDistance(freeStationPosition, robots[0].Position);

            if (busyStationPosition != null)
            {
                int toBusyDistance = DistanceHelper.FindDistance(busyStationPosition, robots[0].Position);

                if (toFreeDistance < toBusyDistance + 30)
                {
                    nextStepPosition = freeStationPosition;
                }
                else
                {
                    nextStepPosition = busyStationPosition;
                }
            }
            Assert.AreEqual(freeStationPosition, nextStepPosition);
        }
    }
}
