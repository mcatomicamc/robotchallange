﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Dmytro.Rudevych.RobotChallange.Test
{
    [TestClass]
    public class CreatingTest
    {
        [TestMethod]
        public void CreateTest()
        {
            var algorithm = new DmytroRudevychAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation());
            map.Stations.Add(new EnergyStation());
            map.Stations.Add(new EnergyStation());
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 600, Position = new Position(2, 3) } };
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }
    }
}
