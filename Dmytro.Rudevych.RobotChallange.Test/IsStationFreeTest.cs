﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Dmytro.Rudevych.RobotChallange;

namespace Dmytro.Rudevych.RobotChallange.Test
{
    [TestClass]
    public class IsStationFreeTest
    {
        [TestMethod]
        public void FreeStationTest()
        {
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) } };
            robots.Add(new Robot.Common.Robot(){Energy = 200, Position = new Position(4,5)});
            var algorithm = new DmytroRudevychAlgorithm();
            var map = new Map();
            var stationPosition = new Position(5, 7);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            Assert.IsTrue(algorithm.IsStationFree(map.Stations[0],robots[0],robots));
        }
    }
}
