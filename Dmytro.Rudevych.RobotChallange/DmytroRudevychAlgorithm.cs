﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;


namespace Dmytro.Rudevych.RobotChallange
{
    public class DmytroRudevychAlgorithm : IRobotAlgorithm
    {
        private const int CREATING_ROBOT_ENERGY = 400;
        private const int ATTACK_ENERGY = 30;
        private const double KOEF = 1.0 / 3.0;

        public string Author
        {
            get { return "Dmytro Rudevych"; }
        }

        public string Description
        {
            get { return "Test"; }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy > CREATING_ROBOT_ENERGY) && (robots.Count < map.Stations.Count))
            {
                return new CreateNewRobotCommand();
            }

            Position stationPosition = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);
            Position busyStationPosition = FindNearestBusyStation(robots[robotToMoveIndex], map, robots);

            if (stationPosition == null)
                return null;
            if ( /*stationPosition == movingRobot.Position*/movingRobot.Position.X >= (stationPosition.X - 1)
                                                            && movingRobot.Position.X <= (stationPosition.X + 1)
                                                            && movingRobot.Position.Y >= (stationPosition.Y - 1)
                                                            && movingRobot.Position.Y <= (stationPosition.Y + 1))
            {
                return new CollectEnergyCommand();
            }
            else
            {
                Position nextStepPosition = new Position();
                int toFreeDistance = DistanceHelper.FindDistance(stationPosition, movingRobot.Position);

                if (busyStationPosition != null)
                {
                    int toBusyDistance = DistanceHelper.FindDistance(busyStationPosition, movingRobot.Position);

                    if (toFreeDistance < toBusyDistance + ATTACK_ENERGY)
                    {
                        nextStepPosition = stationPosition;
                    }
                    else
                    {
                        nextStepPosition = busyStationPosition;
                    }
                }
                else
                {
                    nextStepPosition = stationPosition;
                }

                int distance = DistanceHelper.FindDistance(movingRobot.Position, nextStepPosition);
               
                if (movingRobot.Energy > 0 && distance > movingRobot.Energy)
                {
                    for (int i = 0; i < 50 && distance > movingRobot.Energy && distance >= 1; i++)
                    {
                        nextStepPosition.X = (int) ((movingRobot.Position.X + KOEF * stationPosition.X) / (1 + KOEF));
                        nextStepPosition.Y = (int) ((movingRobot.Position.Y + KOEF * stationPosition.Y) / (1 + KOEF));
                        distance = DistanceHelper.FindDistance(movingRobot.Position, nextStepPosition);
                    }
                }

                return new MoveCommand() {NewPosition = nextStepPosition};
            }
        }

        public bool IsEnemyRobot(Robot.Common.Robot movingRobot, Robot.Common.Robot enemyRobot)
        {
            if (movingRobot.OwnerName == enemyRobot.OwnerName)
                return false;
            else return true;
        }
        
        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        public Position FindNearestBusyStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots) == false)
                {
                    foreach (var robot in robots)
                    {
                        if (IsEnemyRobot(movingRobot, robot))
                        {
                            if (robot.Position == station.Position)
                            {
                                int distance = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                                if (distance < minDistance)
                                {
                                    minDistance = distance;
                                    nearest = station;
                                }
                            }
                        }
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }

        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

    }
}
